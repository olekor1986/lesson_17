<?php
//пространство имен распространяется на Классы, Функции, Константы.

            //варианты объявления простанства имен
/*
         namespace userlib
        {
           ---code---
        }
*/

/*
 *       namespace userlib;
 */

use \userlib\User;
use \userlib\minishop\Cart;

spl_autoload_register('includeClass');

function includeClass($className)
{
    $path = str_replace('\\', '/', $className) . '.php';
    if (file_exists($path)) {
        require_once $path;
    }
}
//другой класс User
/*
class User
{
    public static function sayHello()
    {
        return "Hi I am User from index.php";
    }
}
*/
echo User::sayHello();

echo "<br>";

echo User::sayHello();

echo "<br>";

// \minishop\Cart::getCart(); // - относительное квалифицированное имя

echo Cart::getCart();